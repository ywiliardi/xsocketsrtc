using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using XSockets.Core.Common.Configuration;
using XSockets.Core.Common.Socket;
using XSockets.Core.Configuration;

[assembly: PreApplicationStartMethod(typeof(MvcApplication1.XSocketsWebBootstrapper1), "Start")]

namespace MvcApplication1
{
    public static class XSocketsWebBootstrapper1
    {
        private static IXSocketServerContainer wss;
        public static void Start()
        {

            File.WriteAllText("d://test.txt", DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss"));

            var myCustomConfigs = new List<IConfigurationSetting>();
            myCustomConfigs.Add(new XSocketsConfig());  

            wss = XSockets.Plugin.Framework.Composable.GetExport<IXSocketServerContainer>();
            wss.StartServers(configurationSettings: myCustomConfigs);
        }


    }

    public class XSocketsConfig : ConfigurationSetting
    {
        public XSocketsConfig() : base(new Uri("ws://192.168.3.148:4502/"), new Uri("ws://127.0.0.1:4502")) { }
    }


 
}
