﻿XSockets.Sample.WebRTC is installed

To start the sample do the following

1: Add a new XSockets.Web.Bootstrapper (ctrl+shift+a)
2: Install jQuery (also edit the reference to be the installed version)
3: Under WebRTCSample\Client right click on index.html and select "set as startpage"
4: Right click the project and select properties.
5: Under the "Web" tab go to the "Servers" section and set Use Visual Studio Development Server

Now hit F5

NOTE: Current version supports Chrome only.
Note: Namespaces might look weird under WebRTCSample\Controller since they are just files copied in.